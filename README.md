## 用python获取csdn热榜内容，并用窗体展示，实现转成pdf的功能

#### 介绍
前景提要: 因为python老师布置了一个大作业，爬取某个网站，并用窗体展示，可能内容不是很大，我在gitee上找了很多，没有找到合适自己的.

如果你只是**用python想做个小项目或者只是想完成python课老师布置的大作业**，那我感觉我这个小项目完全可以满足，
如果呢，你已经学的足够多了，那还是请绕道看看别人成熟的开源项目吧。
如果喜欢这个项目的话，还请点一下 star.
#### 依赖的库
pyqt5,requests,matplotlib,parsel,pdfkit,json,excel,fake_useragent

我自己的电脑用的是python虚拟环境 ==> python 3.11

2023-12-2 写 ====> 目前没啥报错

<span style="color:red;">电脑上需要安装wkhtmltopdf.exe</span>,并且需要在settings.py中配置路径为自己的安装路径.

![img_8.png](mdImage%2Fimg_8.png)

官网 ===><a> https://wkhtmltopdf.org/downloads.html </a>

#### 运行截图
初始状态：

![img.png](mdImage%2Fimg.png)

右键菜单：

![img_1.png](mdImage%2Fimg_1.png)

改变选项：

![img_2.png](mdImage%2Fimg_2.png)

上页切换：

![img_3.png](mdImage%2Fimg_3.png)

下页切换：

![img_4.png](mdImage%2Fimg_4.png)

数据保存到excel表格中：

![img_5.png](mdImage%2Fimg_5.png)

生成pdf:

![img_6.png](mdImage%2Fimg_6.png)

右键查看详情：

![img_7.png](mdImage%2Fimg_7.png)

#### 感想
不辜负每一个太阳升起的日子，不辜负身边每一场花开，不辜负身边一点一滴的拥有，用心地去欣赏，去热爱，新的一天又会是一个新的开始。


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
