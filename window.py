"""
主体框体
"""
import sys

from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QPushButton, QVBoxLayout, QGroupBox, QTableWidget, \
     QTableWidgetItem, QAbstractItemView, QMenu, QComboBox, QLabel, QMainWindow
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt
from settings import setting
from util import util
import browser


class MyWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUi()

    def initUi(self):
        """
        初始化窗体
        :return: None
        """
        self.resize(1200, 920)
        articleListGroupBox = QGroupBox("CSDN热榜文章",self)
        articleListGroupBox.setGeometry(10,10,1180,500)
        articleLayout = QVBoxLayout(articleListGroupBox)
        # 分页
        frontPageButton = QPushButton("上一页",self)
        frontPageButton.setGeometry(200,520,100,30)
        frontPageButton.clicked.connect(self.goFrontPage)

        nextPageButton = QPushButton("下一页",self)
        nextPageButton.setGeometry(900,520,100,30)
        nextPageButton.clicked.connect(self.goNextPage)

        # 下拉框
        selectBoxItems = setting.selectItems
        self.selectBox = QComboBox(self)
        for item in selectBoxItems:
            self.selectBox.addItem(item)
        self.selectBox.setGeometry(500,520,160,30)
        self.selectBox.currentIndexChanged.connect(self.selectChanged)

        logGroupBox = QGroupBox("总热度趋势图",self)
        logGroupBox.setGeometry(10,560,1180,370)
        logLayout = QHBoxLayout(logGroupBox)

        # 图片
        self.label = QLabel()
        self.initPicture()
        logLayout.addWidget(self.label)

        self.setWindowTitle(setting.windowTitle)
        self.setWindowIcon(QIcon(r"./icon/csdn.png"))
        # 请求数据
        self.hotContent = util.getAjaxHotContent()["data"]
        hotContent = self.hotContent
        util.draw(hotContent)
        # 表格
        self.table = QTableWidget(len(hotContent), 8)
        self.initTable(hotContent,self.table)
        articleLayout.addWidget(self.table)

        # 保存json数据
        self.save()

        self.show()
        print("[INFO]: init successfully")

    def save(self):
        util.saveArticlesJson(self.hotContent)
        util.saveExcel()
    def initPicture(self):
        pixmap = QtGui.QPixmap(r"./image/image.jpg")
        self.label.setPixmap(pixmap)

    def selectChanged(self,index):
        type = self.selectBox.itemText(index)
        util.setType(type)
        # 请求数据
        self.hotContent = util.getAjaxHotContent()["data"]
        hotContent = self.hotContent
        self.initTable(hotContent, self.table)
        # 刷新图片
        util.draw(hotContent)
        self.initPicture()
        self.save()

    def initTable(self,hotContent,table:QTableWidget):
        table.verticalHeader().hide()
        table.setHorizontalHeaderLabels(setting.headerLabel)
        # 设置列宽度
        table.setColumnWidth(4, 100)
        table.setColumnWidth(3, 120)
        table.setColumnWidth(2, 150)
        table.setColumnWidth(1, 262)
        # 美化
        self.setStyleSheet("selection-background-color:#e67e22;")
        table.setAlternatingRowColors(True)
        table.setStyleSheet("QTableWidget:item{backgroud-color:#f0f0f0;}")
        # 头部
        header = table.horizontalHeader()
        header.setStyleSheet("QHeaderView::section { background-color: #f39c12; color: white; }")
        # 禁止最大化
        self.setFixedSize(self.size())
        # 禁止编辑
        table.setEditTriggers(QTableWidget.NoEditTriggers)
        # 整行选中
        table.setSelectionBehavior(QAbstractItemView.SelectRows)
        # 右键菜单
        table.setContextMenuPolicy(Qt.CustomContextMenu)
        table.customContextMenuRequested.connect(self.generateMenu)
        table.setSelectionMode(QTableWidget.SingleSelection)
        # 插入数据
        for row in range(len(hotContent)):
            table.setItem(row, 0, QTableWidgetItem(str(hotContent[row]["productId"])))
            table.setItem(row, 1, QTableWidgetItem(hotContent[row]["articleTitle"]))
            table.setItem(row, 2, QTableWidgetItem(hotContent[row]["nickName"]))
            table.setItem(row, 3, QTableWidgetItem(hotContent[row]["hotRankScore"]))
            table.setItem(row, 4, QTableWidgetItem(str(hotContent[row]["commentCount"])))
            table.setItem(row, 5, QTableWidgetItem(hotContent[row]["favorCount"]))
            table.setItem(row, 6, QTableWidgetItem(hotContent[row]["viewCount"]))
            table.setItem(row, 7, QTableWidgetItem(hotContent[row]["productType"]))
            table.update()

    def goFrontPage(self):
        # 改变page
        page = 0 if (util.page - 1)<0 else util.page-1
        util.setPagination(page,25)
        # 请求数据
        self.hotContent = util.getAjaxHotContent()["data"]
        hotContent = self.hotContent
        print(hotContent[0])
        self.initTable(hotContent,self.table)
        # 刷新图片
        util.draw(hotContent)
        self.initPicture()
        self.save()


    def goNextPage(self):
        page = util.page + 1
        util.setPagination(page,25)
        # 请求数据
        self.hotContent = util.getAjaxHotContent()["data"]
        hotContent = self.hotContent
        self.initTable(hotContent, self.table)
        # 刷新图片
        util.draw(hotContent)
        self.initPicture()
        self.save()

    def generateMenu(self,pos):
        menu = QMenu()
        showDetail = menu.addAction("查看详情")
        toPdf = menu.addAction("转成pdf")
        action = menu.exec(self.table.mapToGlobal(pos))
        if action == toPdf:
            self.toPdf()
        if action == showDetail:
            row = self.table.selectedItems()[0].row()
            self.showDetailHtml(self.hotContent[row]["articleDetailUrl"],self.hotContent[row]["articleTitle"])

    def toPdf(self):
        # 获取当前行数
        selectItem = self.table.selectedItems()
        # 设置用户禁止对table操作
        self.table.setSelectionMode(QAbstractItemView.NoSelection)
        if selectItem:
            row = selectItem[0].row()
            articleUrl = self.hotContent[row]["articleDetailUrl"]
            # 转成pdf
            title,content = util.parseHtml(util.getDetailHtml(articleUrl))
            util.toPdfFromString(content, f"{util.saveArticle()}\\{title.strip()}.pdf")
            print("[SUCCESS]: successfully convert to pdf, the file in the directory article")
        # 设置用户可以对table操作
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)

    def showDetailHtml(self,url:str,articleTitle:str):
        self.brower = browser.BrowserWindow(url,articleTitle)
        self.brower.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyWindow()
    app.exec()
