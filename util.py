"""
爬取数据
"""

from settings import setting
import matplotlib.pyplot as plt
import parsel
import pdfkit
import requests
import json
from excel import excel
import os



class Util:
    def __init__(self):
        self.page = 2
        self.pageSize = 25
        self.type = "java"
        self.setting = setting
        self.requests = requests

    def setPagination(self, page: int, pageSize: int):
        """
        设置分页
        :param page: 第几页
        :param pageSize: 每页大小
        :return: None
        """
        self.page = page
        self.pageSize = pageSize

    def setType(self,type:str):
        self.type = type

    def getAjaxHotContent(self):
        """
        发送请求热榜数据
        :return: json数据
        """
        res = requests.get(f"{self.setting.url}page={self.page}&pageNum={self.pageSize}&child_channel={self.type}",
                           headers=self.setting.userAgent)
        res.encoding = self.setting.encoding
        return res.json()

    def getDetailHtml(self,articleUrl: str):
        """
        请求文章页面数据
        :param url: 页面地址
        :return: str数据
        """
        res = requests.get(articleUrl,
                           headers=self.setting.userAgent)
        res.encoding = self.setting.encoding
        print("[INFO]: html data request is success")
        return res.text

    def saveArticle(self):
        """
        保存文章
        :return: None
        """
        currentPath = os.getcwd()
        filePath = currentPath+"\\article"
        try:
            os.mkdir(filePath)
            print("[INFO]: 创建路径成功")
        except:
            print("[IGNORE]: article 目录已存在,无需创建")
        finally:
            return filePath

    def parseHtml(self,htmlText:str):
        """
        转换html获取到title和content
        :param htmlText: html字符串
        :return: str
        """
        page = parsel.Selector(htmlText)
        # 文章标题
        title = page.css(".title-article::text").get()
        # 文章内容
        content = page.css("article").get()
        return title,content

    def toPdfFromString(self,string:str,toPdfPath:str):
        """
        从字符串中转换成pdf
        :param string: html字符串
        :param toPdfPath: 保存路径
        :return:
        """
        wHtmlToPdfPath = setting.wHtmlToPdfPath
        config = pdfkit.configuration(wkhtmltopdf=wHtmlToPdfPath)

        # 配置 PDFKit 选项（可根据需要进行调整）
        pdfkit_options = setting.pdfkitOption
        try:
            pdfkit.from_string(string,toPdfPath,configuration=config, options=pdfkit_options)
        except Exception as e:
            print("[ERROR]: error converting string to pdf ")

    def draw(self,hotContent):
        """
        根据ajax请求的数据作图
        :param hotContent: 请求的数据
        :return: None
        """
        x = []
        y = []
        for item in hotContent:
            x.append(str(item["productId"]))
            y.append(int(item["hotRankScore"]))
        plt.figure(figsize=(14,4),dpi=120)
        plt.plot(x,y,color='red',marker='o',linestyle='solid',linewidth=2,markersize=4)
        plt.savefig('./image/image.jpg',dpi=100,bbox_inches='tight')

    def saveArticlesJson(self,hotContent):
        """
        保存json数据到json文件中
        :param hotContent: json数据
        :return: None
        """
        try:
            with open("./json/articles.json", "w", encoding="utf-8") as f:
                json.dump(hotContent, f)
            print("[INFO]: json data is already update")
        except IOError:
            print("[ERROR]: error saving json data")
    def saveExcel(self):
        """

        :return:
        """
        try:
            excel.insertExcel()
            print("[INFO]: excel data is already update")
        except Exception:
            print("[ERROR]: error inserting json data to excel")



util = Util()

# if __name__ == '__main__':
#     util = Util()
#
#     articles = util.getAjaxHotContent()["data"]
#     for article in articles:
#         print(article)
    # result= util.toPdf("https://www.jianshu.com/","out.pdf")
    # print(result)
    # util.saveArticle()
    # title,content = util.parseHtml(util.getDetailHtml("https://blog.csdn.net/xyy1028/article/details/100110814"))
    # util.toPdfFromString(content,f"{os.getcwd()}\\article\\{title.strip()}.pdf")