from settings import setting
import xlwt
import json

class Excel:
    def __init__(self):
        self.titles = setting.headerLabel
    def insertExcel(self):from settings import setting
import xlwt
import json

class Excel:
    def __init__(self):
        self.titles = setting.headerLabel
    def insertExcel(self):
        with open("./json/articles.json", encoding="utf-8") as f:
            self.articles = json.load(f)
        book = xlwt.Workbook()
        sheet = book.add_sheet("CSDN文章")
        for col in range(len(self.titles)):
            sheet.write(0, col, self.titles[col])
        for row in range(len(self.articles)):
            sheet.write(row + 1, 0, self.articles[row]["productId"])
            sheet.write(row + 1, 1, self.articles[row]["articleTitle"])
            sheet.write(row + 1, 2, self.articles[row]["nickName"])
            sheet.write(row + 1, 3, self.articles[row]["hotRankScore"])
            sheet.write(row + 1, 4, self.articles[row]["commentCount"])
            sheet.write(row + 1, 5, self.articles[row]["favorCount"])
            sheet.write(row + 1, 6, self.articles[row]["viewCount"])
            sheet.write(row + 1, 7, self.articles[row]["productType"])
        book.save(setting.excelSavePath)

excel = Excel()