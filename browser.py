from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QIcon
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QMainWindow
"""
browser
"""
class BrowserWindow(QMainWindow):
    def __init__(self, url:str,articleTitle:str):
        super().__init__()
        # 设置新窗口的尺寸
        self.resize(1500, 800)
        self.setWindowTitle(articleTitle)
        self.setWindowIcon(QIcon(r"./icon/csdn.png"))
        # 创建QWebEngineView实例
        self.webview = QWebEngineView(self)
        self.setCentralWidget(self.webview)
        # 加载指定的URL
        self.webview.load(QUrl(url))
