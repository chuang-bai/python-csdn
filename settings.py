"""
配置设置

"""
from fake_useragent import FakeUserAgent


class Setting:

    def __init__(self):
        """
        url:热榜请求路径
        userAgent:用户代理
        encoding:编码方式
        windowTitle:窗体标题
        selectItems:窗体中间选择框的选项
        wHtmlToPdfPath:转pdf的工具的路径
        headerLabel:表格的表头
        pdfkitOption:pdfkit的配置
        """
        self.url = "https://blog.csdn.net/phoenix/web/blog/hot-rank?"
        self.userAgent = {"User-Agent": FakeUserAgent().chrome}
        self.encoding = "utf-8"
        self.windowTitle = "获取csdn文章转成pdf"
        self.selectItems = ['java','python','c','云原生','人工智能','前沿技术','软件工程','后端','javascript','php','大数据','移动开发','嵌入式','游戏','网络','运维']
        self.headerLabel = ["文章id", "文章题目", "作者昵称", "总热度", "评论数", "关注数", "总浏览量", "类型"]
        self.excelSavePath = r"./excel/articles.xlsx"
        self.wHtmlToPdfPath = r'D:\pdfkit\wkhtmltopdf\bin\wkhtmltopdf.exe'
        self.pdfkitOption = {
            'page-size': 'Letter',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding':'utf-8',
            'custom-header': [('Accept-Encoding','gzip')],
            'no-outline':None,
            'dpi': 75,
            'javascript-delay': '2000',
            'image-quality': 500
        }
setting = Setting()
